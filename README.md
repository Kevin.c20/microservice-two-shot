# Wardrobify

Team:

* Kevin - Shoes
* Micah Mershon - Hats microservice

## Design

## Shoes microservice
I made two models BinVO, and Shoe, the shoe model includes details such as manufacturer, name, color, photo, and bin. 
I created two seperate files one for a Shoe lists and one for a shoe form in order to create a new shoe, view a list of shoes and delete shoes from your list. 
<!-- Explain your models and integration with the wardrobe
microservice, here. -->

## Hats microservice

I created two models, Hat and LocationVO. The Hat model has all the required fields,
and its location field forms a many-to-one relationship with the LocationVO model through
use of "models.ForeignKey".

The "poller.py" file found within the "poll" directory of the hats microservice directory
polls the wardrobe microservice for locations and creates LocationVO objects for the hats
microservice to use when creating Hat objects.

I created one file called "HatPage.js" to handle the creation of React compenents to
show a list of hats and their details, create a new hat, and delete a hat all on
the same page. The "Nav.js" was already set up to have its Hats element link to
"http://localhost:3000/hats", so I just set up the connection in "App.js" and that was that.