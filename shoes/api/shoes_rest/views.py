from django.shortcuts import render
from .models import Shoe, BinVO
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "bin_number", "bin_size", "import_href"]
    
class ShoeEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer","name","color","photo", "id", "bin"]
    encoders = {"bin": BinVOEncoder()}

@require_http_methods(["GET", "POST"])
def api_shoe_list(request):
    if request.method =="GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes":shoes},
            encoder =ShoeEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Error"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
@require_http_methods(["DELETE"])
def api_shoe_delete(request, id):
    try:
        shoe = Shoe.objects.get(id=id)
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    except Shoe.DoesNotExist:
        response= JsonResponse({"message":"Error"})
        response.status_code = 404
        return response
    
    
    



# Create your views here.
