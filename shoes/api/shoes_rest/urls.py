from django.urls import path
from .views import api_shoe_list, api_shoe_delete

urlpatterns = [
    path("shoes/", api_shoe_list, name="api_shoe_list"),
    path("shoes/<int:id>/", api_shoe_delete, name="api_shoe_delete"),
]