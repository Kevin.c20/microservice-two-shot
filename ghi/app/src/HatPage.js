import React, {useEffect, useState} from "react";


function HatForm (props) {
  const [locations, setLocations] = useState([]);
  const [fabric, setFabric] = useState('');
  const [styleName, setStyleName] = useState('');
  const [color, setColor] = useState('');
  const [pictureUrl, setPictureUrl] = useState('');
  const [location, setLocation] = useState('');

  const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};

      data.fabric = fabric;
      data.style_name = styleName;
      data.color = color;
      data.picture_url = pictureUrl;
      data.location = location;

      const hatUrl = 'http://localhost:8090/api/hats/';
      const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
              'Content-Type': 'application/json',
          }
      };

      const response = await fetch(hatUrl, fetchConfig);
      if (response.ok) {
          const newHat = await response.json();
          console.log(`New hat was created successfully at ${newHat.href}`);
          props.fetchHats();

          setFabric('');
          setStyleName('');
          setColor('');
          setPictureUrl('');
          setLocation('');
      }
  }

  const handleFabricChange = (event) => {
      const value = event.target.value;
      setFabric(value);
  }

  const handleStyleNameChange = (event) => {
      const value = event.target.value;
      setStyleName(value);
  }

  const handleColorChange = (event) => {
      const value = event.target.value;
      setColor(value);
  }

  const handlePictureUrlChange = (event) => {
      const value = event.target.value;
      setPictureUrl(value);
  }

  const handleLocationChange = (event) => {
      const value = event.target.value;
      setLocation(value);
  }

  const fetchData = async () => {
      const url = 'http://localhost:8100/api/locations/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
      }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
          <div className="shadow p-4 mt-4">
              <h1>Create a new hat</h1>
              <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                  <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                  <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                  <input onChange={handleStyleNameChange} placeholder="Style" required type="text" name="style_name" id="style_name" className="form-control"/>
                  <label htmlFor="style_name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                  <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                  <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                  <input onChange={handlePictureUrlChange} placeholder="URL" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                  <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                  <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                  <option value="">Closet Name - Section Number/Shelf Number</option>
                  {locations.map(location => {
                      return (
                          <option key={location.href} value={location.href}>
                              {location.closet_name} - {location.section_number}/{location.shelf_number}
                          </option>
                      );
                  })}
                  </select>
              </div>
              <button className="btn btn-primary">Create</button>
              </form>
          </div>
  );
}



function HatColumn(props) {
  function handleButtonClick() {
    props.list.map(async data => {
      const hat = data;
      const url = `http://localhost:8090${hat.href}`
      const response = await fetch(url, {
        method: 'delete'
      });
      if (response.ok) {
        console.log(`Hat at ${hat.href} has been deleted`)
        props.fetchData();
      }
    });
  }
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                return (
                    <div key={hat.href} id={hat.href} className="card mb-3 shadow" style={{ border: '2px solid black' }}>
                    <img src={hat.picture_url} className="card-img-top" />
                    <div className="card-body">
                        <p className="card-title">
                            Fabric: {hat.fabric}
                        </p>
                        <p className="card-text">
                            Style: {hat.style_name}
                        </p>
                        <p className="card-text">
                            Color: {hat.color}
                        </p>
                    </div>
                    <div className="card-footer">
                        <p>
                            Closet: {hat.location.closet_name}
                        </p>
                        <p>
                            Section: {hat.location.section_number}
                        </p>
                        <p>
                            Shelf: {hat.location.shelf_number}
                        </p>
                    </div>
                        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center" style={{ marginBottom: '10px' }}>
                            <button onClick={handleButtonClick} type="button" className="btn btn-danger">DELETE</button>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

const HatPage = (props) =>  {
    const [hatColumns, setHatColumns] = useState([[], [], []]);
  
    const fetchData = async () => {
      const url = 'http://localhost:8090/api/hats/';
  
      try {
        const response = await fetch(url);
        if (response.ok) {

          const data = await response.json();
  
          const requests = [];
          for (let hat of data.hats) {
            const detailUrl = `http://localhost:8090${hat.href}`;
            requests.push(fetch(detailUrl));
          }

          const responses = await Promise.all(requests);
  
          const columns = [[], [], []];
  
          let i = 0;
          for (const hatResponse of responses) {
            if (hatResponse.ok) {
              const details = await hatResponse.json();
              columns[i].push(details);
              i += 1;
              if (i > 2) {
                i = 0;
              }
            } else {
              console.error(hatResponse);
            }
          }
  
          setHatColumns(columns);
        }
      } catch (e) {
        console.error(e);
      }
    }
  
    useEffect(() => {
      fetchData();
    }, []);
  
    return (
      <>
        <div className="px-4 py-5 my-5 mt-0 text-center bg-info">
          <h1 className="display-5 fw-bold">Hats!</h1>
          <div className="col-lg-6 mx-auto">
            <p className="lead fw-bold mb-4">
                We have every hat in the world right here!
            </p>
            <HatForm fetchHats={fetchData} />
          </div>
        </div>
        <div className="container">
          <h2>List of Hats:</h2>
          <div className="row">
            {hatColumns.map((hatList, index) => {
              return (
                <HatColumn key={index} list={hatList} fetchData={fetchData} />
              );
            })}
          </div>
        </div>
      </>
    );
}

export default HatPage;
