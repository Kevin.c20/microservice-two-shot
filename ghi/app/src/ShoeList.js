import React, {useEffect, useState} from "react";

function ShoesList(props) {
    const deleteShoe = async (id) => {
        fetch(`http://localhost:8080/api/shoes/${id}`, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(() => {
            window.bin.reload();
        })
    }

    return (
        <>
            <table className="table table-hover table-dark table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Name</th>
                        <th>Color</th>
                        <th>Photo</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map(shoe => {
                        return (
                            <tr key={shoe.bin.id}>
                                <td>{shoe.manufacturer}</td>
                                <td>{shoe.name}</td>
                                <td>{shoe.color}</td>
                                <td><img src={shoe.photo} /></td>
                                <td><button onClick={() => deleteShoe(shoe.id)} type="button" className="btn btn-outline-light">Delete </button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ShoesList;

 